package com.all4dev.commitinserter.deamon;

import com.all4dev.commitinserter.A4DConfig;

import java.sql.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;

public class DBInserter extends Thread {

    private BlockingQueue<Push> blockingQueue;

    public DBInserter(BlockingQueue<Push> blockingQueue) {
        setDaemon(true);
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        System.out.println("DBInserter - Thread Start");
        Connection dbConnection = null;
        PreparedStatement selectPreparedStatement = null;
        PreparedStatement insertPreparedStatement = null;
        try {
            Class.forName(A4DConfig.JDBC_DRIVER);
            dbConnection = DriverManager.getConnection(A4DConfig.DB_URL, A4DConfig.DB_USER, A4DConfig.DB_PASS);
            String selectTaskSQL = "SELECT tasks.id\n" +
                    "FROM tasks\n" +
                    "INNER JOIN projects ON projects.id        = tasks.projects_id\n" +
                    "INNER JOIN users    ON projects.leader_id = users.id\n" +
                    "\n" +
                    "WHERE users.username LIKE ?" +
                    "AND   projects.name LIKE ?" +
                    "AND   CONCAT(LCASE(tasks.`type`), '/', tasks.branch_name) LIKE ?" +
                    "LIMIT 1";
            selectPreparedStatement = dbConnection.prepareStatement(selectTaskSQL);

            String insertCommitSQL = "INSERT INTO `commits` " +
                    "(`hash`, `subject`, `author_name`, `author_email`, `date`, `push_uuid`, `tasks_id`) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?);";
            insertPreparedStatement = dbConnection.prepareStatement(insertCommitSQL);
            while (!isInterrupted()) {
                try {
                    System.out.println("DBInserter - Thread Wait for New");
                    Push push = blockingQueue.take();

                    selectPreparedStatement.setString(1, push.getRepoUsername());
                    selectPreparedStatement.setString(2, push.getRepoName());
                    selectPreparedStatement.setString(3, push.getBranch());

                    ResultSet rs = selectPreparedStatement.executeQuery();
                    if (rs.first()) {
                        long tasksId = rs.getLong(1);

                        String uuid = UUID.randomUUID().toString();
                        for (Commit commit : push.getCommits()) {
                            insertPreparedStatement.setString(1, commit.getHash());
                            insertPreparedStatement.setString(2, commit.getSubject());
                            insertPreparedStatement.setString(3, commit.getAuthorName());
                            insertPreparedStatement.setString(4, commit.getAuthorEmail());
                            insertPreparedStatement.setString(5, commit.getDate().substring(0, 10) + "T" + commit.getDate().substring(11, 19) + commit.getDate().substring(20));
                            insertPreparedStatement.setString(6, uuid);
                            insertPreparedStatement.setLong(7, tasksId);
                            insertPreparedStatement.executeUpdate();
                        }
                        System.out.println("--------------------------------------------------------------------------------------");
                        System.out.println("Repo: " + push.getRepoUsername() + "/" + push.getRepoName());
                        System.out.println("On branch " + push.getBranch());
                        System.out.println("Insert " + push.getCommits().size() + " commits");
                        System.out.println("--------------------------------------------------------------------------------------");
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("DBInserter - Date MalFormated");
                } catch (InterruptedException e) {
                    System.out.println("DBInserter - Interrupt");
                    break;
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new RuntimeException();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (insertPreparedStatement != null) {
                try {
                    insertPreparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("DBInserter - Thread End");
        }
    }
}
