package com.all4dev.commitinserter.deamon;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Main {

    private static DBInserter dbInserter;
    private static MemCacheReader memCacheReader;

    public static void main(String[] args) {
        System.out.println("Start Main");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println(" ");
                System.out.println("Shutdown Signal");
                close();
            }
        });
        BlockingQueue blockingQueue = new ArrayBlockingQueue<Push>(10);
        dbInserter = new DBInserter(blockingQueue);
        dbInserter.setUncaughtExceptionHandler((t, e) -> {
            close();
        });
        dbInserter.start();

        memCacheReader = new MemCacheReader(blockingQueue);
        memCacheReader.run();
    }

    public static void close() {

        System.out.println("Interrupt DB Inserter");
        if (dbInserter != null)
            dbInserter.interrupt();

        System.out.println("Shutdown MemCached Client");
        if (memCacheReader != null)
            memCacheReader.shutDown();

        try {
            dbInserter.join();
        } catch (InterruptedException e) {
        }
    }
}
