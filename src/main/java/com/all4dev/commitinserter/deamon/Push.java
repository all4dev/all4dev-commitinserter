package com.all4dev.commitinserter.deamon;

import java.util.List;

public class Push {
    private String repoUsername;
    private String repoName;
    private String branch;
    private List<Commit> commits;

    public String getRepoUsername() {
        return repoUsername;
    }

    public String getRepoName() {
        return repoName;
    }

    public String getBranch() {
        return branch;
    }

    public List<Commit> getCommits() {
        return commits;
    }
}
