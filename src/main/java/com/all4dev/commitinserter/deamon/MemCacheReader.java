package com.all4dev.commitinserter.deamon;

import com.all4dev.commitinserter.A4DConfig;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.redisson.RedissonShutdownException;
import org.redisson.client.RedisConnectionException;
import org.redisson.client.RedisException;
import org.redisson.core.RQueue;

import java.util.concurrent.BlockingQueue;

public class MemCacheReader implements Runnable {
    private Config config;
    private RedissonClient client;

    private BlockingQueue<Push> blockingQueue;

    public MemCacheReader(BlockingQueue<Push> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    public void run() {
        try {
            config = new Config();
            config.setUseLinuxNativeEpoll(true);
            config.useSingleServer().setAddress(A4DConfig.REDIS_IP + ":" + A4DConfig.REDIS_PORT);
            client = Redisson.create(config);
            RQueue<String> queue = client.getQueue(A4DConfig.REDIS_QUEUE);

            Gson gson = new Gson();
            while (true) {
                try {
                    String json = queue.poll();
                    if (json != null) {
                        Push push = gson.fromJson(json, Push.class);
                        System.out.println("Redis - Add to Blocking Queue");
                        blockingQueue.add(push);
                    }
                    Thread.sleep(1000);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                } catch (InterruptedException | RedisException e) {
                    if (!(e.getCause() != null && e.getCause() instanceof RedissonShutdownException))
                        e.printStackTrace();
                    break;
                }
            }
        } catch (RedisConnectionException e) {
            throw e;
        } finally {
            shutDown();
        }
        System.out.println("Redis - Run End");
    }

    public void shutDown() {
        if (client != null && !client.isShutdown() && !client.isShuttingDown()) {
            System.out.println("Redis - Client Shutdown");
            client.shutdown();
            System.out.println("Redis - Client Shutdown: Complete");
        }
    }
}
