package com.all4dev.commitinserter.deamon;

public class Commit {
    private String hash;
    private String subject;
    private String authorName;
    private String authorEmail;
    private String date;

    public String getHash() {
        return hash;
    }

    public String getSubject() {
        return subject;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public String getDate() {
        return date;
    }
}
