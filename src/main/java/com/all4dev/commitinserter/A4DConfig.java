package com.all4dev.commitinserter;

public class A4DConfig {
    public static final String REDIS_IP = "127.0.0.1";
    public static final String REDIS_PORT = "6379";
    public static final String REDIS_QUEUE = "commit_queue";


    public static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://127.0.0.1/all4dev";

    public static final String DB_USER = "all4dev";
    public static final String DB_PASS = "all4dev";
}