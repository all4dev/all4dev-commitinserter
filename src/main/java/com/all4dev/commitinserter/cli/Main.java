package com.all4dev.commitinserter.cli;

import com.all4dev.commitinserter.A4DConfig;
import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.redisson.core.RQueue;

public class Main {

    public static void main(String[] args) {
        if (args.length == 1) {
            Config config = null;
            RedissonClient client = null;
            try {
                config = new Config();
                config.setUseLinuxNativeEpoll(true);
                config.useSingleServer().setAddress(A4DConfig.REDIS_IP + ":" + A4DConfig.REDIS_PORT);
                client = Redisson.create(config);

                RQueue<String> queue = client.getQueue(A4DConfig.REDIS_QUEUE);
                queue.add(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (client != null) {
                    client.shutdown();
                }
            }
        }
    }
}
